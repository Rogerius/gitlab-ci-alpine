# Alpine Linux Docker image for GitLab CI

A minimal Docker image based on Alpine Linux for bash scripting, 
Python unittesting, and handling GitLab API from bash.

## Features

### Core tools

| **Feature** | **Packages** |
| --- | --- |
| Bash tools | bash, gawk, sed, grep, bc, coreutils |
| SSH Client | openssh-client |
| Git | git |
| Python 2.7 | py-pip |

### Mail sending

| **Feature** | **Packages** |
| --- | --- |
| SSMTP | ssmtp, mailx |

### Python testing tools

| **Feature** | **Packages (PIP)** |
| --- | --- |
| Dependencies | setuptools |
| Test tools | green |
| Coverage | coverage |

### GitLab Bash API

| **Feature**  | **Packages** |
| --- | --- |
| Dependencies | [bor-sh-infrastructure/libsaas_gitlab](https://gitlab.com/bor-sh-infrastructure/libsaas_gitlab) |
| 'git-gitlab' | [Rogerius/git-gitlab](https://gitlab.com/Rogerius/git-gitlab) |

> NOTE: It's a forked version of bor-sh's git-gitlab.
> MR's filtering behaviour is different from the original.
      
### Entrypoint
/bin/bash (instead of /bin/sh)