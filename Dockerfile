FROM alpine:latest

MAINTAINER Ádám Keserű <keseru.adam@gmail.com>

# Base packages
RUN set -xe \
# installing core alpine packages
      && apk -v add --update --no-cache bash gawk sed grep bc coreutils curl \
# installing required development packages
      && apk -v add --update --no-cache openssh-client git py-pip

# Mailer setup
RUN set -xe \
# installing mailer packages
      && apk -v add --update --no-cache ssmtp mailx 

# Python testsuite with coverage
RUN set -xe \
# installing required python packages
      && pip install --no-cache-dir setuptools \
      && pip install --no-cache-dir mock coverage green

# 'git gitlab'
RUN set -xe \
# installing required python packages
      && pip install --no-cache-dir libsaas \
# cloning 'git-gitlab' and requirements
      && cd /tmp/ \
      && git clone https://gitlab.com/bor-sh-infrastructure/libsaas_gitlab.git \
      && git clone https://gitlab.com/Rogerius/git-gitlab.git \
# installing 'git-gitlab' and requirements
      && cd libsaas_gitlab \
      && python setup.py install \
      && cd ../git-gitlab \
      && python setup.py install \
# adding alias for 'git-gitlab'
      && git config --global alias.gitlab lab \
# cleanup
      && cd /tmp/ \
      && rm -rf libsaas_gitlab \
      && rm -rf git-gitlab

CMD ["/bin/bash"]

